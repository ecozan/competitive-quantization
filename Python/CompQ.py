# -*- coding: utf-8 -*-

from RVQ import RVQ
import numpy as np
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans


class CompQ(RVQ):
    
    def __init__(self,K=256,M=8,L=4):
        self.L = L
        self.learningRate = np.ones(M)*0.5
        for m in range(M):
            self.learningRate[m] /= np.ceil(np.log2(m+1)+1)
        self.learningRate /= np.sum(self.learningRate)
        self.learningRate *= 0.5
        print self.learningRate
            
        super(CompQ,self).__init__(K,M)
    
    def __initializeCodebooks__(self,data):
        residuals = np.copy(data)
        nBits = int(np.log2(self.K))
        self.codebooks = []
        
        for m in range(self.M):
            pca = PCA(n_components=nBits)
            pca.fit(residuals)
            transforms = pca.transform(residuals)

            weights = np.log2(pow(pca.explained_variance_,0.5))
            clusterCenters = []
            codelengths = np.zeros((nBits))
            
            for j in range(nBits):
                maxAt = np.argmax(weights)
                codelengths[maxAt]+=1
                weights[maxAt]-=1
            
            for j in range(nBits):
                if(codelengths[j]==0):
                    break
                kmeans = KMeans(int(pow(2,codelengths[j])))
                kmeans.fit(transforms[:,j].reshape(-1,1)) 
                clusterCenters.append(kmeans.cluster_centers_)
                
            nDimensions = len(clusterCenters)
            print [m, nDimensions]
            vProjectedCentroids = np.zeros((self.K,nBits))    
            for k in range(self.K):
                ind = k
                vInd = np.zeros((nDimensions))
                for j in range(nDimensions):
                    vInd[j] = ind % len(clusterCenters[j])
                    ind = np.floor(ind / len(clusterCenters[j]))
                    
                for j in range(nDimensions):
                    vProjectedCentroids[k][j] = clusterCenters[j][int(vInd[j])];
            
            codebook = np.zeros((self.K,len(data[0])))
            for k in range(self.K):
                codebook[k][:] = pca.inverse_transform(vProjectedCentroids[k])
            self.codebooks.append(codebook)
            
            distances = np.zeros((self.K))
            for i in range(len(residuals)):
                for k in range(self.K):
                    distances[k]=self.euc_dist(residuals[i],codebook[k])
                residuals[i]-=codebook[np.argmin(distances)]
            
    def __encodeInTrain__(self,sample):
        residuals = np.zeros((self.L,len(sample)))
        residualdists = np.zeros((self.L,1))
        codes = np.zeros((self.L,self.M),dtype='int')

        tmpdists = np.zeros((self.K))
        for k in range(self.K):
            tmpdists[k] = self.euc_dist(sample,self.codebooks[0][k])

        for l in range(self.L):
            minAt = np.argmin(tmpdists)
            codes[l][0] = minAt
            residuals[l] = sample-self.codebooks[0][minAt]
            residualdists[l] = tmpdists[minAt]*tmpdists[minAt]
            tmpdists[minAt] = 10000000
            
        tmpdists = np.zeros((self.K*self.L,1))     
            
        for m in range(1,self.M):
            
            tmpresiduals = np.copy(residuals)
            tmpresidualdists = np.copy(residualdists)
            tmpcodes = np.copy(codes)

            for l in range(self.L):
                for k in range(self.K):
                    tmpdists[l*self.K+k] = self.euc_dist(residuals[l],self.codebooks[m][k]) 
            
            for l in range(self.L):
                minAt = np.argmin(tmpdists)
                r = int(minAt/self.K)
                k = minAt % self.K
                
                tmpcodes[l] = codes[r]
                tmpcodes[l][m] = k
                tmpresiduals[l] = residuals[r]-self.codebooks[m][k]
                tmpresidualdists[l] = tmpdists[minAt]*tmpdists[minAt]
                tmpdists[minAt] = 10000000
                    
            residuals = np.copy(tmpresiduals)
            residualdists = np.copy(tmpresidualdists)
            codes = np.copy(tmpcodes)     
            bestInd = np.argmin(residualdists)    
        return codes[bestInd], residuals[bestInd]


    def encode(self,sample):
        residuals = [None] * int(self.L)
        codes = [None] * int(self.L)
        for l in range(self.L):
             codes[l] = [None] * int(self.M)

        residualdists = np.zeros((self.L))
        tmpdists = np.zeros((self.K))
        for k in range(self.K):
            tmpdists[k] = self.euc_dist(sample,self.codebooks[0][k])

        for l in range(self.L):
            minAt = np.argmin(tmpdists)
            codes[l][0] = minAt
            residuals[l] = sample-self.codebooks[0][minAt]
            residualdists[l] = tmpdists[minAt]*tmpdists[minAt]
            tmpdists[minAt] = 10000000
            
        tmpdists = np.zeros((self.K*self.L,1))     

        for m in range(1,self.M):
            tmpresiduals = np.copy(residuals)
            tmpresidualdists = np.copy(residualdists)
            tmpcodes = np.copy(codes)
            m1 = np.arange(m)
            
            dotProducts = np.dot(self.codebooks[m],sample)
            ##distance calculation with look-ups
            for k in range(self.K):
                centroidNorm = self.offlineTable[m][k][m][k]
                for l in range(self.L):
                    centroidDot = 0
                    for m1 in range(m):
                        centroidDot += self.offlineTable[m1][codes[l][m1]][m][k]
                    residualDot = dotProducts[k]-centroidDot
                    tmpdists[l*self.K+k] = residualdists[l] + centroidNorm -2*residualDot 

            for l in range(self.L):
                minAt = np.argmin(tmpdists)
                r = int(minAt/self.K)
                k = minAt % self.K
                
                tmpcodes[l] = np.copy(codes[r])
                tmpcodes[l][m] = k
                tmpresiduals[l] = residuals[r]-self.codebooks[m][k]
                tmpresidualdists[l] = tmpdists[minAt]
                tmpdists[minAt] = 10000000
                    
            residualdists = np.copy(tmpresidualdists)
            
            residuals = tmpresiduals[:]
            codes = tmpcodes[:]     

        return codes[np.argmin(residualdists)]


    def train(self,data):
      
        samples = np.copy(data)
        self.__initializeCodebooks__(samples)
        
        nIter = 150
        for i in range(nIter):
            np.random.shuffle(samples)
            dError = 0
            
            for d in samples:
                code, error = self.__encodeInTrain__(d)
                dError += np.power(np.linalg.norm(error),2)
                for m in range(self.M):
                    self.codebooks[m][code[m]] += self.learningRate[m]*error

            print dError/len(samples)                
            self.learningRate*=0.99

        self.createOfflineTable()
        
            
