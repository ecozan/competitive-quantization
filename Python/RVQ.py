#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  7 10:57:41 2017

@author: ozan
"""

from sklearn.cluster import KMeans
import numpy as np

class RVQ(object):
    
    def __init__(self, K=256,M=8):
        self.K = K
        self.M = M
        self.codebooks = []
        self.offlineTable = []
        self.queryTable = []
        self.queryNorm = []

        
    def train(self,data):
        residuals = np.copy(data)
        for m in range(self.M):
            kmeans = KMeans(n_clusters=self.K).fit(residuals) 
            codebook = kmeans.cluster_centers_
            codes = kmeans.labels_
            self.codebooks.append(codebook)
            for i in range(len(residuals)):
                residuals[i] -= codebook[codes[i]]
        self.__createOfflineTable__()
        

    def encode(self,sample):
        residual = np.copy(sample)
        code = np.zeros((self.M),dtype='int')
        distances = np.zeros((self.K))
        for m in range(self.M):
            for k in range(self.K):
                distances[k]=np.linalg.norm(residual-self.codebooks[m][k])
            code[m]= np.argmin(distances)
            residual -= self.codebooks[m][int(code[m])]
        return code
    

    def reconstruct(self,code):
        sample = np.copy(self.codebooks[0][int(code[0])])
        for m in range(1,self.M):
            sample += self.codebooks[m][int(code[m])]
        return sample


    def setQuery(self,query):
        self.queryTable = [None] * int(self.M)
        for m in range(self.M):
            self.queryTable[m] = [None] * int(self.K)
            for k in range(self.K):
                self.queryTable[m][k]=-2*np.dot(query,self.codebooks[m][k])
        self.queryNorm = np.dot(query,query).item()
        
    
    def asymmetricDistance(self,code):
        dist = 0
        for m1 in range(self.M):
            for m2 in range(self.M):
                dist += self.offlineTable[m1][code[m1]][m2][code[m2]]
        for m in range(self.M):
            dist += self.queryTable[m][code[m]]
        dist += self.queryNorm
        return dist
    
    
    def save(self, path):
        np.savez(path,M=self.M,K=self.K,codebooks = self.codebooks)
    
    
    def load(self, path):
        savefile = np.load(path)
        self.M = savefile['M'] 
        self.K = savefile['K']
        self.codebooks = savefile['codebooks']
        self.__createOfflineTable__()


    def __createOfflineTable__(self):
        self.offlineTable = []
        for m1 in range(self.M):
            self.offlineTable.append([])
            for k1 in range(self.K):
                self.offlineTable[m1].append([])
                for m2 in range(self.M):
                    self.offlineTable[m1][k1].append([])
                    for k2 in range(self.K):
                        self.offlineTable[m1][k1][m2].append(np.dot(self.codebooks[m1][k1],self.codebooks[m2][k2]))
    
        
    
    