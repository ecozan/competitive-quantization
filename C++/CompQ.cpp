/*
 * CompQ.cpp
 *
 *  Created on: 24 May 2017
 *      Author: ozan
 */

#include "CompQ.h"
#include "TransformCoding.h"
#include <algorithm>
#include <iostream>

CompQ::CompQ(int k, int l)
:RVQ(k,l),m_nLevel(32)
{
	double dLearningRate = 0.5;
	m_vLearningRate.resize(l,dLearningRate);
}

CompQ::~CompQ()
{
}


void CompQ::Initialize(const vector<TSample>& vSamples)
{
	m_vCentroids.clear();
	m_vCentroids.resize(m_nL);
	int nBitsPerLayer = log2(m_nK);
	vector<TSample> vResiduals = vSamples;

	// for each layer...
	for(int l=0;l<m_nL;l++)
	{
		TransformCoding tc(nBitsPerLayer);
		tc.Train(vResiduals);
		vector<vector<double> > vCenters = tc.GetCenters();

		// from TC centers create initial centroids
		vector<int> vInd(vCenters.size(),0);
		vector<vector<double> > vProjectedCentroids(m_nK);
		for(int j=0;j<m_nK;j++)
		{
			vProjectedCentroids[j].resize(nBitsPerLayer,0);

			int ind = j;
			for(int k=vInd.size()-1;k>=0;k--)
			{
				vInd[k] = ind % vCenters[k].size();
				ind = floor(ind/vCenters[k].size());
			}

			for(int k=0;k<vCenters.size();k++)
				vProjectedCentroids[j][k] = vCenters[k][vInd[k]];
		}

		// transform procjected centroids to the original space
		PCA pca = tc.GetPCA();
		m_vCentroids[l].resize(m_nK);
		for(int j=0;j<m_nK;j++)
			m_vCentroids[l][j] = pca.Revert(vProjectedCentroids[j]);

		// update residuals
		for(int j=0;j<vResiduals.size();j++)
		{
			//find the winner centroid...
			int ind = 0;
			double dist = 10000000;
			for(int k=0;k<m_nK;k++)
			{
				double tmpdist = L2(vResiduals[j],m_vCentroids[l][k]);
				if(tmpdist<dist)
				{
					dist = tmpdist;
					ind = k;
				}
			}
			vResiduals[j] -= m_vCentroids[l][ind];
		}
	}
}

void CompQ:: Train(const vector<TSample>& vSamples)
{
	Initialize(vSamples);

	int nIterations = 250;

	m_vLearningRate.resize(m_nL);

	double dLearningRate = m_vLearningRate[0];
	for(int i=1;i<=m_nL;i++)
		m_vLearningRate[i-1] /= ceil(log2(i)+1);
	m_vLearningRate /= L1(m_vLearningRate);
	m_vLearningRate *= dLearningRate;

	cout<<m_vLearningRate<<endl;

	for(int iter=nIterations;iter>0;iter--)
	{
/*      // Uncomment to see the decrease in the quantization error per iteration
		cout<<"iter: "<<iter<<" ";
		double totalError = 0;
		CreateOfflineTable();
		for(int i=0;i<vSamples.size();i++)
		{
			TCode code = Encode(vSamples[i]);
			TSample vReconstructed = Reconstruct(code);
			TSample vError = vSamples[i] - vReconstructed;
			totalError += pow(L2(vError),2);
		}
		cout<<" QE: "<<totalError/vSamples.size()<<endl;
*/
		vector<int> vIndices = ShuffledIndices(vSamples.size());
		m_vLearningRate*=(double(iter)/(iter+1));
//		m_vLearningRate*=(0.99);

		for(int i=0;i<vSamples.size();i++)
		{
			int ind = vIndices[i];
			TCode code = EncodeInTrain(vSamples[ind]);
			TSample vReconstructed = Reconstruct(code);
			TSample vError = vSamples[ind] - vReconstructed;

			for(int l=0;l<m_nL;l++)
				m_vCentroids[l][code[l]] += m_vLearningRate[l]*(vError);
		}
	}

	CreateOfflineTable();
}

vector<int> CompQ::ShuffledIndices(int n) const
{
	vector<pair<double,int> > vIndexPairs(n);

	for(int i=0;i<n;i++)
	{
		vIndexPairs[i].first = ((double) rand() / (RAND_MAX));
		vIndexPairs[i].second = i;
	}

	sort(vIndexPairs.begin(),vIndexPairs.end());

	vector<int> vIndices(n);
	for(int i=0;i<n;i++)
		vIndices[i] = vIndexPairs[i].second;

	return vIndices;
}

Quantizer::TCode CompQ::Encode(const TSample& vSample) const
{
	vector<TResidual> vResiduals(m_nLevel,TResidual(100000000,TCode(m_nL,0),vSample));
	//first layer
	{
		vector<double> vDistances(m_nK);
		for(int i=0;i<m_nK;i++)
			vDistances[i] = L2(vSample,m_vCentroids[0][i]);

		// find the minimum nLevel distances
		for(int r=0;r<m_nLevel;r++)
		{
			double dist = 100000000;
			int minInd = 0;

			for(int i=0;i<m_nK;i++)
				if(vDistances[i]<dist)
				{
					minInd = i;
					dist = vDistances[i];
				}
			vResiduals[r].dist = dist*dist;
			vResiduals[r].code[0] = minInd;
			vResiduals[r].sample -= m_vCentroids[0][minInd];
			vDistances[minInd] = 100000001;
		}
	}

	vector<double> vDistances(m_nLevel*m_nK,0);
	vector<TResidual> vTmpResiduals;
	// other layers
	for(int l=1;l<m_nL;l++)
	{
		vector<double> vDotXC(m_nK);
		for(int i=0;i<m_nK;i++)
			vDotXC[i]=Dot(vSample,m_vCentroids[l][i]);
		vTmpResiduals = vResiduals;

		for(int i=0;i<m_nLevel*m_nK;i++)
		{
			int r = i/m_nK;
			int k = i%m_nK;

			double prevDist = vResiduals[r].dist;
			double centroidNorm = m_vOfflineTable[l*m_nK+k][l*m_nK+k];

			double centroidDot = 0;
			for(int kl=0;kl<l;kl++)
				centroidDot += m_vOfflineTable[kl*m_nK + vResiduals[r].code[kl]][l*m_nK+k];

			double residualDot = vDotXC[k]-centroidDot;

			double dist2 = prevDist + centroidNorm - 2*residualDot;

			vDistances[i] = dist2;
		}

		// find the minimum nLevel distances
		for(int r=0;r<m_nLevel;r++)
		{
			double dist = 100000000;
			pair<int,int> minInd(0,0);

			for(int i=0;i<m_nLevel*m_nK;i++)
				if(vDistances[i]<dist)
				{
					minInd.first = i/m_nK;
					minInd.second = i%m_nK;
					dist = vDistances[i];
				}

			vTmpResiduals[r] = vResiduals[minInd.first];
			vTmpResiduals[r].sample -= m_vCentroids[l][minInd.second];
			vTmpResiduals[r].code[l] = minInd.second;
			vTmpResiduals[r].dist = dist;
			vDistances[minInd.first*m_nK+minInd.second] = 100000001;
		}

		vResiduals = vTmpResiduals;
	}

	double tmpMin = 100000000;
	int minInd = 0;
	for(int k=0;k<m_nLevel;k++)
		if(vResiduals[k].dist<tmpMin)
		{
			tmpMin = vResiduals[k].dist;
			minInd = k;
		}

	return vResiduals[minInd].code;
}

// Parallelized version of encoding for training
Quantizer::TCode CompQ::EncodeInTrain(const TSample& vSample) const
{
	int nThreads = 8;
	vector<TResidual> vResiduals(m_nLevel,TResidual(100000000,TCode(m_nL,0),vSample));
	//first layer
	{
		vector<double> vDistances(m_nK);
#pragma omp parallel for num_threads(nThreads)
		for(int i=0;i<m_nK;i++)
			vDistances[i] = L2(vSample,m_vCentroids[0][i]);

		// find the minimum nLevel distances
		for(int r=0;r<m_nLevel;r++)
		{
			double dist = 100000000;
			int minInd = 0;

			for(int i=0;i<m_nK;i++)
				if(vDistances[i]<dist)
				{
					minInd = i;
					dist = vDistances[i];
				}
			vResiduals[r].dist = dist;
			vResiduals[r].code[0] = minInd;
			vResiduals[r].sample -= m_vCentroids[0][minInd];
			vDistances[minInd] = 100000001;
		}
	}

	vector<double> vDistances(m_nLevel*m_nK,0);
	vector<TResidual> vTmpResiduals;
	// other layers
	for(int l=1;l<m_nL;l++)
	{
		vTmpResiduals = vResiduals;

#pragma omp parallel for num_threads(nThreads)
		for(int i=0;i<m_nLevel*m_nK;i++)
		{
			int r = i/m_nK;
			int k = i%m_nK;
			vDistances[i] = L2(vResiduals[r].sample,m_vCentroids[l][k]);
		}

		// find the minimum nLevel distances
		for(int r=0;r<m_nLevel;r++)
		{
			double dist = 100000000;
			pair<int,int> minInd(0,0);

			for(int i=0;i<m_nLevel*m_nK;i++)
				if(vDistances[i]<dist)
				{
					minInd.first = i/m_nK;
					minInd.second = i%m_nK;
					dist = vDistances[i];
				}


			vTmpResiduals[r] = vResiduals[minInd.first];
			vTmpResiduals[r].sample -= m_vCentroids[l][minInd.second];
			vTmpResiduals[r].code[l] = minInd.second;
			vTmpResiduals[r].dist = dist;
			vDistances[minInd.first*m_nK+minInd.second] = 100000001;
		}

		vResiduals = vTmpResiduals;
	}

	double tmpMin = 100000000;
	int minInd = 0;
	for(int k=0;k<m_nLevel;k++)
		if(vResiduals[k].dist<tmpMin)
		{
			tmpMin = vResiduals[k].dist;
			minInd = k;
		}

	return vResiduals[minInd].code;
}
