/*
 * RVQ.cpp
 *
 *  Created on: 23 May 2017
 *      Author: ozan
 */

#include "RVQ.h"
#include "KMeans.h"
#include <iostream>

RVQ::RVQ(int k, int l)
:Quantizer(),m_nL(l),m_nK(k)
{
	cout<<"K: "<<m_nK<<" L: "<<m_nL<<endl;
}

RVQ::~RVQ()
{
}

string RVQ::QuantizerTypeName() const
{
	return string("RVQ");
}

double RVQ::AsymmetricDistance(const TCode& code1) const
{
	// calculate q²-2<q,x>+x²
	double dDist = m_dQueryNorm;

	for(int i=0;i<code1.size();i++)
		dDist+= m_vQueryTable[i][code1[i]];

	for(int i=0;i<code1.size();i++)
		for(int j=0;j<code1.size();j++)
			dDist+=m_vOfflineTable[i*m_nK+code1[i]][j*m_nK+code1[j]];

	return sqrt(dDist);
}

void RVQ::Train(const vector<TSample>& vSamples)
{
	vector<TSample> vResiduals = vSamples;

	cout<<"k "<<m_nK<<" l "<<m_nL<<endl;
	m_vCentroids.resize(m_nL);
	for(int i=0;i<m_nL;i++)
	{

		KMeans kmns(m_nK,true);
		kmns.Train(vResiduals);
		m_vCentroids[i] = kmns.GetCentroids();

		double dEntropy = 0;
		for(int j=0;j<m_nK;j++)
		{
			double p = double(kmns.GetPopulations()[j])/vSamples.size();
//			double p = double(kmns.GetClusterPopulation(j))/vSamples.size();
			dEntropy += p*log(p)/log(2);
		}
		cout<<"Entropy at level "<<i<<" "<<dEntropy<<endl;
		vector<int> vLabels = kmns.GetLabels();
		for(int j=0;j<vResiduals.size();j++)
			vResiduals[j]-=m_vCentroids[i][vLabels[j]];
	}

	CreateOfflineTable();
}

Quantizer::TCode RVQ::Encode(const TSample& vSample) const
{
	int nLayers = m_vCentroids.size();

	Quantizer::TCode aCode;
	TSample vResidual = vSample;
	for(int i=0;i<nLayers;i++)
	{
		double minDist = 100000000;
		double ind = 0;
		for(int j=0;j<m_vCentroids[i].size();j++)
		{
			double dist = L2(vResidual,m_vCentroids[i][j]);
			if(dist<minDist)
			{
				minDist=dist;
				ind=j;
			}
		}
		aCode.push_back(ind);
		vResidual-=m_vCentroids[i][ind];
	}

	return aCode;
}

Quantizer::TSample RVQ::Reconstruct(const TCode& code1) const
{
	TSample vReconstructed(m_vCentroids[0][0].size(),0);
	for(int i=0;i<code1.size();i++)
		vReconstructed+=m_vCentroids[i][code1[i]];
	return vReconstructed;
}

void RVQ::Write(const string& file) const
{
	BinaryOut output(file);
	output.Write(QuantizerTypeName()) ;
	output.Write(m_nK);
	output.Write(m_nL);
	output.Write(m_vCentroids);
}


void RVQ::Read(const string& file)
{
	BinaryIn input(file);
	string strMark ;
	input.Read(strMark) ;
	if(strMark != QuantizerTypeName())
		throw Error("[SRVQ] SPCRVQ header <"+ QuantizerTypeName() +">not found in " + file) ;

	input.Read(m_nK) ;
	input.Read(m_nL) ;
	input.Read(m_vCentroids) ;

	CreateOfflineTable();
}

void RVQ::CreateOfflineTable()
{
	m_vOfflineTable.clear();
	m_vOfflineTable.resize(m_nL*m_nK);
	for(int i=0;i<m_vOfflineTable.size();i++)
		m_vOfflineTable[i].resize(m_nK*m_nL);

	for(int i=0;i<m_nL;i++)
		for(int j=0;j<m_vCentroids[i].size();j++)
		{
			for(int l=0;l<m_nL;l++)
				for(int k=0;k<m_vCentroids[l].size();k++)
					m_vOfflineTable[i*m_nK+j][l*m_nK+k]=Dot(m_vCentroids[i][j],m_vCentroids[l][k]);
		}
}

void RVQ::CreateQueryTable(TSample& vSample)
{
	m_vQueryTable.clear();
	m_vQueryTable.resize(m_nL);
	for(int i=0;i<m_nL;i++)
		m_vQueryTable[i].resize(m_nK);

	for(int i=0;i<m_nL;i++)
		for(int j=0;j<m_vCentroids[i].size();j++)
		{
			m_vQueryTable[i][j]= -2*Dot(vSample,m_vCentroids[i][j]);
		}

	m_dQueryNorm = pow(L2(vSample),2);
}


void RVQ::SetQuery(const TSample& vSample)
{
	m_vQuery = vSample;
	CreateQueryTable(m_vQuery);
}

