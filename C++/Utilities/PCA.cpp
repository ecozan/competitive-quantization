/*
 * PCA.cpp
 *
 *  Created on: 13.10.2015
 *      Author: ozan
 */

#include "PCA.h"
#include "BinaryIn.h"
#include "BinaryOut.h"

PCA::PCA()
{
	m_bIsAnalyzeDone = false;
	m_nM = -1;
	m_nD = 0;
}

PCA::~PCA()
{
}

void PCA::Analyze(const vector<vector<double> >& vSamples, int m)
{
	int n = vSamples.size();
	m_nD = vSamples[0].size();
	m_nM = m;


	arma::mat armaMat(n,m_nD);
	for(int i=0;i<n;i++)
		for(int j=0;j<m_nD;j++)
			armaMat(i,j)=vSamples[i][j];

	m_vCenter = arma::mean(armaMat.t(),1);

	arma::mat covariance = arma::cov(armaMat);
	arma::vec eigval;
	arma::mat eigvec;
	arma::eig_sym(eigval, eigvec, covariance);

	m_vWeights.clear();
	m_vWeights.resize(eigval.size());

	// eigval ascending principalweigths descending order
	for(int i=0;i<m_vWeights.size();i++)
		m_vWeights[i]=eigval[eigval.size()-1-i];

	if(m == -1)
		m_nM = eigvec.n_cols;
	else
		m_nM = m;

	m_mPC = arma::mat(m_nM,m_nD);
	for(int i=0;i<m_nD;i++)
		for(int j=0;j<m_nM;j++)
			m_mPC(j,i)=eigvec(i,eigvec.n_cols-1-j);

	// Set flag
	m_bIsAnalyzeDone = true;
}

vector<double> PCA::Transform(const vector<double>& vSamples) const
{
	arma::vec v1(vSamples);
	arma::vec vT = m_mPC*(v1-m_vCenter);
	return vector<double>(vT.mem,vT.mem+vT.size());
}

vector<double> PCA::Revert(const vector<double>& vSamples) const
{
	arma::vec v1(vSamples);
	arma::vec vT = m_mPC.t()*v1+m_vCenter;
	return vector<double>(vT.mem,vT.mem+vT.size());
}

void PCA::Read(const string& inFile)
{
	BinaryIn in(inFile);
	vector<double> vtmp;

	in.Read(m_nD);
	in.Read(m_nM);

	in.Read(vtmp);
	m_vCenter.clear();
	m_vCenter.resize(m_nD);
	for(int i=0;i<m_nD;i++)
		m_vCenter[i]=vtmp[i];
	vtmp.clear();
	in.Read(vtmp);

	m_vWeights.clear();
	m_vWeights.resize(m_nD);
	for(int i=0;i<m_nD;i++)
		m_vWeights[i]=vtmp[i];

	m_mPC.clear();
	m_mPC = arma::mat(m_nM,m_nD);
	for(int i=0;i<m_nM;i++)
	{
		vtmp.clear();
		in.Read(vtmp);
		for(int j=0;j<m_nD;j++)
			m_mPC(i,j)=vtmp[j];
	}

	m_bIsAnalyzeDone = true;
}

void PCA::Write(const string& outFile) const
{
	BinaryOut out(outFile.c_str());
	vector<double> vtmp;

	out.Write(m_nD);
	out.Write(m_nM);

	vtmp.clear();
	vtmp.resize(m_nD);
	for(int i=0;i<m_nD;i++)
		vtmp[i]=m_vCenter[i];
	out.Write(vtmp);

	vtmp.clear();
	vtmp.resize(m_nD);
	for(int i=0;i<m_nD;i++)
		vtmp[i]=m_vWeights[i];
	out.Write(vtmp);

	for(int i=0;i<m_nM;i++)
	{
		vtmp.clear();
		vtmp.resize(m_nD);
		for(int j=0;j<m_nD;j++)
			vtmp[j]=m_mPC(i,j);
		out.Write(vtmp);
	}
}




