/*!
**************************************************************************************
 * \file BinaryIn.h
 *
 * \brief
 *    Definition of BinaryIn.
 *
 * \author
 * 	  Ezgi Can Ozan
 *************************************************************************************
 */

#pragma once

#include <fstream>
#include <vector>
#include "Error.h"
#include "inttypes.h"

using namespace std ;


class BinaryIn
{
public:

	explicit BinaryIn() ;
	explicit BinaryIn(const string& binaryFile) ;
	~BinaryIn() ;

	/// Opens given file.
	void Open(const string& binaryFile) ;
	/// Closes opened file.
	void Close() ;

	/// Reads bytes from the binary input file
	const vector<uint8_t> ReadBytes();

	/// Reads a castable type
	template<typename T>
	void Read(T& data)
	{
		CheckOpen() ;
		m_in.read(reinterpret_cast<char*>(&data), sizeof(data)) ;
		if(!m_in)
			throw Error("[BinaryIn] Reading fails.") ;

	}

	/// Reads a vector of a castable type
	template<typename T>
	void Read(vector<T> & data)
	{
		CheckOpen() ;
		size_t size = -1 ;
		m_in.read(reinterpret_cast<char*>(&size), sizeof(size_t)) ;
		data.resize(size) ;
		m_in.read(reinterpret_cast<char*>(&data[0]), sizeof(T) * size) ;
		if(!m_in)
			throw Error("[BinaryIn] Reading fails.") ;
	}

	/// Reads a vector of vector of a castable type
	template<typename T>
	void Read(vector<vector<T> >& data)
	{
		CheckOpen() ;
		size_t size = -1 ;
		m_in.read(reinterpret_cast<char*>(&size), sizeof(size_t)) ;
		data.resize(size) ;
		for(int i = 0 ; i < int(data.size()) ; i += 1)
			Read(data[i]) ;
	}

	/// Reads a string
	void Read(string& data)
	{
		CheckOpen() ;
		size_t size = -1 ;
		m_in.read(reinterpret_cast<char*>(&size), sizeof(size_t)) ;
		data.resize(size) ;
		m_in.read(reinterpret_cast<char*>(&data[0]), sizeof(char) * size) ;
		if(!m_in)
			throw Error("[BinaryIn] Reading fails.") ;
	}

	/// Removes a file from the file system
	static void Remove(const string& file);


private:

	/// Throws exception if file is not open
	void CheckOpen() ;

	/// The input file
	ifstream m_in ;
} ;
