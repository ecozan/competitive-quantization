/*!
**************************************************************************************
 * \file Error.cpp
 *
 * \brief
 *    Implementation of Error .
 *************************************************************************************
 */

#include "Error.h"

Error::Error(const string& strMessage) throw()
{
	m_strMessage = strMessage ;
}

Error::~Error() throw()
{}

const char* Error::what() const throw()
{
	return m_strMessage.c_str() ;
}

