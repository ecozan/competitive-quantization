/*
 * TemplateUtilities.h
 *
 *  Created on: 23.6.2016
 *      Author: ozan
 */

#ifndef UTILITIES_TEMPLATEUTILITIES_H_
#define UTILITIES_TEMPLATEUTILITIES_H_
/**
 *
 *  Template utilities class, which declares and implements many template functions
 *  such as vector operations and distance calculations
 */


#include <vector>
#include <math.h>
#include <iostream>

#include "Error.h"


namespace utilities
{


	/// operator overload for vector display with cout
	template<typename T>
	ostream& operator <<(ostream& out, const vector<T>& v)
	{
		out << "[ " ;
		for(typename vector<T>::const_iterator i = v.begin() ; i != v.end() ; ++i)
		{
			if(i != v.begin())
				out << ", " ;
			out << *i ;
		}
		out << " ]" ;
		return out ;
	}

	/// Returns the dot product between two vectors.
	template<typename T>
	double Dot(const vector<T>& v1, const vector<T>& v2)
	{
		double dDotProduct = 0;
		for(int i = 0 ; i < v1.size() ; i++)
			dDotProduct += v1[i] * v2[i] ;
		return dDotProduct;
	}

	/// Returns the L2 distance between two vectors.
	template<typename T>
	double L2(const vector<T>& v1, const vector<T>& v2)
	{
		double dL2 = 0;
		for(int i = 0 ; i < v1.size(); i++)
			dL2 += pow((v1[i] - v2[i]),2) ;
		return sqrt((double)(dL2));
	}

	/// Returns L2 norm of a vector.
	template<typename T>
	double L2(const vector<T>& v1)
	{
		double dL2 = 0;
		for(int i = 0 ; i < v1.size(); i++)
			dL2 += (v1[i])*(v1[i]) ;
		return sqrt((double)(dL2));
	}

	/// Returns L1 norm of a vector.
	template<typename T>
	double L1(const vector<T>& v1)
	{
		double dL2 = 0;
		for(int i = 0 ; i < v1.size() ; i++)
			dL2 += (v1[i]);
		return (dL2);
	}

	/// vector addition
	template<typename T>
	const vector<T> operator +(const vector<T>& v1, const vector<T>& v2)
	{
		vector<T> vResult(v1) ;
		for(int i=0;i<v1.size();i++)
			vResult[i]+=v2[i];
		return vResult ;
	}

	/// vector subtraction
	template<typename T>
	const vector<T> operator -(const vector<T>& v1, const vector<T>& v2)
	{
		vector<T> vResult(v1) ;
		for(int i=0;i<v1.size();i++)
			vResult[i]-=v2[i];
		return vResult ;
	}

	/// multiplication of a vector by a scalar
	template<typename T>
	const vector<T> operator *(const T& c1, const vector<T>& v2)
	{
		vector<T> vResult(v2) ;
		for(int i=0;i<v2.size();i++)
			vResult[i]*=c1;
		return vResult ;
	}

	/// division of a vector by a scalar
	template<typename T>
	const vector<T> operator /(const vector<T>& v2, const T& c1)
	{
		vector<T> vResult(v2) ;
		for(int i=0;i<v2.size();i++)
			vResult[i]/=c1;
		return vResult ;
	}

	/// in place addition for a vector
	template<typename T>
	void operator +=(vector<T>& v1, const vector<T>& v2)
	{
		for(int i = 0 ; i < v1.size() ; i++)
			v1[i] += v2[i] ;
	}

	/// in place subtraction for a vector
	template<typename T>
	void operator -=(vector<T>& v1, const vector<T>& v2)
	{
		for(int i = 0 ; i < v1.size() ; i++)
			v1[i] -= v2[i] ;
	}

	/// in place multiplication of a vector by a scalar
	template<typename T>
	void operator *=(vector<T>& v1, const T& v2)
	{
		for(int i = 0 ; i < v1.size() ; i++)
			v1[i] *= v2 ;
	}

	/// in place division of a vector by a scalar
	template<typename T>
	void operator /=(vector<T>& v1, const T& v2)
	{
		for(int i = 0 ; i < v1.size() ; i++)
			v1[i] /= v2 ;
	}

	/// returns the index of the maximum in a vector
	template<typename T>
	int MaxAt(const vector<T>& v1)
	{
		T max = v1[0];
		int maxAt = 0;
		for(int i=1;i<v1.size();i++)
			if(v1[i]>max)
			{
				max = v1[i];
				maxAt = i;
			}
		return maxAt;
	}
};

#endif /* UTILITIES_TEMPLATEUTILITIES_H_ */
