/*!
**************************************************************************************
 * \file BinaryOut
 *
 * \brief
 *    Definition of BinaryOut.
 *
 * \author
 * 	  Ezgi Can Ozan
 *************************************************************************************
 */

#pragma once

#include <fstream>
#include <vector>
#include "inttypes.h"

using namespace std ;

class BinaryOut
{
public:

	explicit BinaryOut() ;
	explicit BinaryOut(const string& binaryFile) ;
	~BinaryOut() ;

	/// Opens given file.
	void Open(const string& binaryFile) ;
	/// Closes opened file.
	void Close() ;

	/// Writes bytes to the binary file
	void WriteBytes(const vector<uint8_t>& vBytes) ;

	/// Writes a castable type
	template<typename T>
	void Write(const T& data)
	{
		CheckOpen() ;
		m_out.write(reinterpret_cast<const char*>(&data), sizeof(data)) ;
	}

	/// Writes a vector of a castable type
	template<typename T>
	void Write(const vector<T>& data)
	{
		CheckOpen() ;
		size_t size = data.size();
		m_out.write(reinterpret_cast<const char*>(&size), sizeof(size)) ;
		m_out.write(reinterpret_cast<const char*>(&data[0]), sizeof(T) * size) ;
	}

	/// Writes a vector of vector of a castable type
	template<typename T>
	void Write(const vector<vector<T> >& data)
	{
		CheckOpen() ;
		size_t size = data.size();
		m_out.write(reinterpret_cast<const char*>(&size), sizeof(size)) ;
		for(int i=0;i<size;i++)
			Write(data[i]) ;
	}

	/// Writes a string
	void Write(const string& data)
	{
		CheckOpen() ;
		size_t size = data.length();
		m_out.write(reinterpret_cast<const char*>(&size), sizeof(size)) ;
		m_out.write(reinterpret_cast<const char*>(&data[0]), sizeof(char) * size) ;
	}


	/// Creates a temporary file and returns its path
	static string TemporaryFile();


private:

	/// Throws exception if file is not open
	void CheckOpen()  ;

	/// the binary output file
	ofstream m_out ;
} ;
