/*
 * PCA.h
 *
 *  Created on: 13.10.2015
 *      Author: ozan
 */

#ifndef UTILITIES_PCA_H_
#define UTILITIES_PCA_H_

#include "Error.h"
#include <armadillo>

/**
 *  Implementation of Principal Component Analysis
 *
 */

class PCA
{
public:
	PCA();
	~PCA();

	/// Perform analysis on given dataset, d defines the dimension reduction
	void Analyze(const vector<vector<double> >& vSamples, int d = -1);

	/// Transform a given sample
	vector<double> Transform(const vector<double>& vSamples) const;

	/// Revert a transformed sample to the original space
	vector<double> Revert(const vector<double>& vSamples) const;

	/// Returns principal weights
	vector<double> GetPrincipalWeights(){return m_vWeights;}

	/// Returns transformation matrix
	arma::mat GetTransformationMatrix() const {return m_mPC;}

	/// Returns the affine shift vector
	arma::mat GetMean() const {return m_vCenter;}

	/// Reads a saved PCA object
	void Read(const string& inFile);

	/// Saves a PCA object
	void Write(const string& outFile) const;

private:

	/// PC matrix, (!!principal component vectors are stored in rows!!)
	arma::mat m_mPC;

	/// The affine shift vector
	arma::vec m_vCenter;

	/// Principal weights
	vector<double> m_vWeights;

	/// Number of dimensions for the original space
	int m_nD;

	/// Number of dimensions after dimension reduction
	int m_nM;

	/// Controls if the PCA object analysed a dataset before
	bool m_bIsAnalyzeDone;
};



#endif /* UTILITIES_PCA_H_ */
