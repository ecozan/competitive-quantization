/*!
**************************************************************************************
 * \file BinaryIn.cpp
 *
 * \brief
 *    Implementation of BinaryIn.
 *************************************************************************************
 */

#include "BinaryIn.h"
#include "Error.h"
#include <stdint.h>
#include <unistd.h>

BinaryIn::BinaryIn()
{}

BinaryIn::BinaryIn(const string& binaryFile)
{
	Open(binaryFile) ;
}

BinaryIn::~BinaryIn()
{
	Close() ;
}

void BinaryIn::CheckOpen()
{
	if(not m_in.is_open())
		throw Error("[BinaryIn] Open a file before writing.") ;
}

void BinaryIn::Open(const string& binaryFile)
{
	m_in.open(binaryFile.c_str(), ios::in | ios::binary) ;

	if(!m_in)
	{
		m_in.clear() ;
		throw Error("[BinaryIn] Can't open file for binary read: " + binaryFile) ;
	}
}

void BinaryIn::Close()
{
	if(m_in.is_open())
		m_in.close() ;
	m_in.clear() ;
}

const vector<uint8_t> BinaryIn::ReadBytes()
{
	CheckOpen() ;

	size_t nCurrent = m_in.tellg();
	m_in.seekg(0, std::ios::end) ;
	size_t nLast = m_in.tellg();
	m_in.seekg(nCurrent, std::ios::beg) ;
	size_t nByteCount = nLast - nCurrent ;
	vector<uint8_t> vBytes(nByteCount) ;

	m_in.read(reinterpret_cast<char*>(&vBytes[0]), nByteCount) ;
	return vBytes ;
}

void BinaryIn::Remove(const string& file)
{
	remove(file.c_str());
}
