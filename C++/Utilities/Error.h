/*!
**************************************************************************************
 * \file Error.h
 *
 * \brief
 *    Definition of Error class.
 *
 * \author
 *    - Ezgi Can Ozan
 *************************************************************************************
 */

#pragma once

#include <exception>
#include <string>
using namespace std;

class Error : public exception
{
public:

	Error(const string& strMessage) throw() ;
	virtual ~Error() throw() ;
	virtual const char* what() const throw() ;

protected:
	string m_strMessage ;
};
