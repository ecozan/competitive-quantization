/*!
**************************************************************************************
 * \file BinaryOut.cpp
 *
 * \brief
 *    Implementation of BinaryOut.
 *
         
 *************************************************************************************
 */

#include "BinaryOut.h"
#include "Error.h"
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>

BinaryOut::BinaryOut()
{
}

BinaryOut::BinaryOut(const string& binaryFile)
{
	Open(binaryFile) ;
}

BinaryOut::~BinaryOut()
{
	Close() ;
}

void BinaryOut::CheckOpen()
{
	if(not m_out.is_open())
		throw Error("[BinaryOut] Open a file before writing.") ;
}

void BinaryOut::Open(const string& binaryFile)
{
	m_out.open(binaryFile.c_str(), ios::out | ios::binary) ;

	if(not m_out)
	{
		m_out.clear() ;
		throw Error("[BinaryOut] Can't open file for binary write: " + binaryFile) ;
	}
}

void BinaryOut::Close()
{
	if(m_out.is_open())
		m_out.close() ;
	m_out.clear() ;
}

void BinaryOut::WriteBytes(const vector<uint8_t>& vBytes)
{
	CheckOpen() ;
	m_out.write(reinterpret_cast<const char*>(&vBytes[0]), vBytes.size()) ;
}

string BinaryOut::TemporaryFile()
{
	string tmp = "/tmp/Utilities_temp_XXXXXX";
	int nFile = -1 ;
	nFile = mkstemp(&tmp[0]) ;
	if(nFile >= 0)
		close(nFile) ;
	else
		throw Error("Temporary File cannot be created");

	return tmp;
}
