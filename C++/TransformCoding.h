/*
 * TransformCoding.h
 *
 *  Created on: 24 May 2017
 *      Author: ozan
 */

#ifndef TRANSFORMCODING_H_
#define TRANSFORMCODING_H_
#include "Quantizer.h"
#include "Utilities/PCA.h"

/**
 * Implementation of Transform Coding for ANN
 *
 * Brandt, J., 2010, June.
 * Transform coding for fast approximate nearest neighbor search in high dimensions.
 * In Computer Vision and Pattern Recognition (CVPR), 2010 IEEE Conference on (pp. 1815-1822). IEEE.
 *
 */

class TransformCoding : public Quantizer
{
public:

	/*!
		 * \param j subspace dimension
	 */
	TransformCoding(int j=64);
	/// Destructor
	~TransformCoding();
	/// Return Quantizer type name
	/// Train the quantizer using given set of samples.
	virtual void Train(const vector<TSample>& vSamples);

	/// Returns the codewords of a sample.
	virtual TCode Encode(const TSample& vSample) const;

	virtual TSample Reconstruct(const TCode& code1) const;

	/// Returns the asymmetric distance between a preset query and a code.
	virtual double AsymmetricDistance(const TCode& code1) const;

	/// Reads a quantizer from a binary file.
	virtual void Read(const string& file);

	/// Writes the quantizer to a binary file.
	virtual void Write(const string& file) const ;

	/// Returns a string representation for the name of the quantizer type.
	virtual string QuantizerTypeName() const;

	/// Sets cached query
	virtual void SetQuery(const TSample& vSample);

	vector<vector<double> > GetCenters() const {return m_vCenters;}
	PCA GetPCA() const {return m_PCA;}

protected:
	int m_nJ;
	PCA m_PCA;
	vector<int> m_vCodeLengths;
	// for Kmeans
	vector<vector<double> > m_vCenters;
	double m_dSubspaceDist;

	/// Calculate code length per dimension...
	void CalculateCodeLenghts();
	/// Using Greedy method
	void Greedy(const vector<double>& vPrincipalWeights);

	void CalculateCentersLloyd(const vector<TSample>& vSamples);
	/// Find the closest KMeans Center
	TCode LloydQuantization(const TSample& vSample) const;

};





#endif /* TRANSFORMCODING_H_ */
