/*
 * MaxLloydQuantizer.h
 *
 *  Created on: 23.6.2016
 *      Author: ozan
 */

#ifndef MAXLLOYD_H_
#define MAXLLOYD_H_

#include <vector>

using namespace std;

/**
 *  1-D Clustering Method that implements MaxLloyd's Algorithm.
 *  Instead of calculating PDFs, it uses a K-Means like approach
 */

class MaxLloyd
{
public:
	/// Constructor requires the number of desired clusters
	MaxLloyd(int k);
	~MaxLloyd();

	/// Train with 1-D samples
	void Train(const vector<double>& vSamples);

	/// Get the center points for m_nK Clusters
	vector<double> GetCenters(){return m_vCenters;}

private:
	int m_nK;
	vector<double> m_vCenters;
};



#endif /* MAXLLOYDQUANTIZER_H_ */
