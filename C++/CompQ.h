/*
 * CompQ.h
 *
 *  Created on: 24 May 2017
 *      Author: ozan
 */

#ifndef COMPQ_H_
#define COMPQ_H_

/**
 * Implementation of Competitive Quantization for ANN
 *
 *
 * Ozan, E. C., Kiranyaz, S. and Gabbouj, M. (2016).
 * Competitive Quantization for Approximate Nearest Neighbor Search.
 * IEEE Transactions on Knowledge and Data Engineering, 28(11), 2884-2894.
 *
 */

#include "RVQ.h"
class CompQ : public RVQ
{
public:

	/// Constructor
	CompQ(int k=256,int l=8);
	/// Destructor
	~CompQ();

	/// Returns the codewords of a sample.
	virtual TCode Encode(const TSample& vSample) const;
	/// Train the quantizer using given set of samples.
	virtual void Train(const vector<TSample>& vSamples);


protected:
	/// Parallelised version of encode for training, which does not use look-up tables
	virtual TCode EncodeInTrain(const TSample& vSample) const;
	/// Initializes the centroids for training
	void Initialize(const vector<TSample>& vSamples);
	/// Shuffles the sample indices for training
	vector<int> ShuffledIndices(int n) const;

	int m_nLevel;
	vector<double> m_vLearningRate;
};



#endif /* COMPQ_H_ */
