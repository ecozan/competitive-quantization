/*
 * RVQ.h
 *
 *  Created on: 23 May 2017
 *      Author: ozan
 */

#ifndef RVQ_H_
#define RVQ_H_

/**
 * Implementation of Residual Vector Quantization for ANN
 *
 * Chen, Y., Guan, T. and Wang, C., 2010.
 * Approximate nearest neighbor search by residual vector quantization.
 * Sensors, 10(12), pp.11259-11273.
 *
 */

#include "Quantizer.h"
class RVQ :public Quantizer
{
public:

	/// Constructor
	RVQ(int k = 256, int l = 8);

	/// Destructor
	virtual ~RVQ();

	/// Trains the quantizer using given set of samples.
	virtual void Train(const vector<TSample>& vSamples);

	/// Returns the codewords of a sample.
	virtual TCode Encode(const TSample& vSample) const;

	/// Reconstructs a vector from a given code
	virtual TSample Reconstruct(const TCode& code1) const;

	/// Returns the asymmetric distance between a preset query and a code.
	virtual double AsymmetricDistance(const TCode& code1) const;

	/// Reads a quantizer from a binary file.
	virtual void Read(const string& file);

	/// Writes the quantizer to a binary file.
	virtual void Write(const string& file) const;

	/// Returns a string representation for the name of the quantizer type.
	virtual string QuantizerTypeName() const;

    /// Sets cached query
	virtual void SetQuery(const TSample& vSample);

protected:

	/// The structure which keeps the residual information for efficient encoding
	class TResidual
	{
		public:
			TResidual(double d, const TCode& c, const TSample& s)
			:dist(d),code(c),sample(s){}
			~TResidual(){}
		double dist;
		TCode code;
		TSample sample;
	};

	/// number of layers
	int m_nL;

	/// number of centroids per layer
	int m_nK;

	/// vector of centroids on each layer
	vector<vector<TSample> > m_vCentroids;

	/// Look-up Table, which holds the dot products between the query and codevectors
	vector<vector<double> > m_vQueryTable;

	/// Look-up Table, which holds the dot products of codevectors
	vector<vector<double> > m_vOfflineTable;

	/// The L2 norm of the given query
	double m_dQueryNorm;

	/// Creates the offline Look-Up Table
	virtual void CreateOfflineTable();

	/// Creates the query Look-Up Table
	virtual void CreateQueryTable(TSample& vSample);
};

#endif /* RVQ_H_ */
