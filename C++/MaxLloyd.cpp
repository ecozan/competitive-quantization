/*
 * MaxLloyd.cpp
 *
 *  Created on: 23.6.2016
 *      Author: ozan
 */
#include <algorithm>
#include <math.h>

#include "MaxLloyd.h"
#include "Utilities/TemplateUtilities.h"

MaxLloyd::MaxLloyd(int k)
{
	m_nK = k;
}
MaxLloyd::~MaxLloyd()
{

}

void MaxLloyd::Train(const vector<double>& vSamples)
{
	vector<double> vTmpSamples = vSamples;
	sort(vTmpSamples.begin(),vTmpSamples.end());

	// initialize centers
	int nStep = vTmpSamples.size()/(m_nK+1);
	m_vCenters.clear();
	m_vCenters.resize(m_nK,0);
	for(int i=0;i<m_nK;i++)
		m_vCenters[i]=vTmpSamples[(i+1)*nStep];
	int nIter = 100;

	vector<int> vClusterIndices(vSamples.size(),0);
	for(int it=0;it<nIter;it++)
	{
		// Update clusters
		for(int i=0;i<vSamples.size();i++)
		{
			double dMin = 100000000;
			int indMin = 0;
			for(int j=0;j<m_nK;j++)
				if(fabs(vSamples[i]-m_vCenters[j])<dMin)
				{
					dMin = fabs(vSamples[i]-m_vCenters[j]);
					indMin = j;
				}

			vClusterIndices[i] = indMin;
		}
		// Update centers
		m_vCenters.clear();
		m_vCenters.resize(m_nK,0);
		vector<int> vClusterPopulation(m_nK,0);
		for(int i=0;i<vSamples.size();i++)
		{
			vClusterPopulation[vClusterIndices[i]]++;
			m_vCenters[vClusterIndices[i]]+=vSamples[i];
		}

		for(int i=0;i<m_nK;i++)
			m_vCenters[i]/=vClusterPopulation[i];
	}
}
