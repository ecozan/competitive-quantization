/*
 * KMeans.h
 *
 *  Created on: 19 May 2017
 *      Author: ozan
 */

#ifndef KMEANS_H_
#define KMEANS_H_

/**
 *
 *  MultiThread enabled implementation of K-Means Clustering Algorithm
 *
 */

#include "Quantizer.h"

class KMeans
{
public:

	/// Constructor
	KMeans(int k, bool multiThread = false );

	/// Destructor
	virtual ~KMeans();

	/// Trains the K-Means
	virtual void Train(const vector<Quantizer::TSample>& vSamples, int nIterations=50);

	/// Returns labels
	vector<int> GetLabels() const{return m_vClusterLabels;}

	/// Returns cluster populations
	vector<int> GetPopulations() const{return m_vCentroidPopulation;}

	/// Returns cluster centroids
	vector<Quantizer::TSample> GetCentroids() const {return m_vCentroids;}

protected:

	/// Data structure for multithreaded implementation of K-Means
	struct distArgs
	{
		const vector<Quantizer::TSample>* vSamples;
		const vector<Quantizer::TSample>* vCentroids;
		int nBegin;
		int nEnd;
		vector<int> vClusterLabels;
		double dSSD;
	};

	/// Initializes centroids with KMeans++
	void InitializeCentroids(const vector<Quantizer::TSample>& vSamples);

	/// Initializes centroids with randomly
	void InitializeCentroidsRandom(const vector<Quantizer::TSample>& vSamples);

	/// Updates cluster indices
	virtual double UpdateClusterIndices(const vector<Quantizer::TSample>& vSamples);

	/// Updates cluster centroids
	virtual void UpdateClusterCentroids(const vector<Quantizer::TSample>& vSamples);

	/// Calculates the distance to a centroid
	void DistanceToCentroid(distArgs& args ) const;

	/// Number of clusters
	int m_nK;

	/// is multithreading enabled?
	bool m_bMultiThread;

	/// Total clustering error
	double m_dSSD;

	/// Cluster labels
	vector<int> m_vClusterLabels;

	/// Cluster centroids
	vector<Quantizer::TSample> m_vCentroids;

	/// Cluster populations
	vector<int> m_vCentroidPopulation;

};



#endif /* KMEANS_H_ */
