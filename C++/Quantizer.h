/*!
*************************************************************************************
 * \file Quantizer.h
 *
 * \brief
 *    Definition of Quantizer base class.
 *
 * \author
 *    - Ezgi Can Ozan
 *************************************************************************************
 */

#pragma once

#include "Utilities/BinaryIn.h"
#include "Utilities/BinaryOut.h"
#include "Utilities/TemplateUtilities.h"

#include <vector>
#include <math.h>
#include <stdlib.h>
#include <string>

using namespace std ;
using namespace utilities;

/**
 * Abstract parent class for quantizers
 */
class Quantizer
{
public:
	/// Format of observations.
	typedef vector<double> TSample ;

	/// Type of core code.
	typedef unsigned char TCodeCore ;

	/// Format of quantization codes.
	typedef vector<TCodeCore> TCode ;

	/// Constructor.
	explicit Quantizer() ;

	/// Destructor.
	virtual ~Quantizer() ;

	/// Returns the asymmetric distance between a preset query and a list of codes.
	const vector<double> AsymmetricDistance(const vector<TCode>& vCodes) const;

	/// Returns the asymmetric distance between a sample and a code.
	double ReconstructedAsymmetricDistance(const TCode& code1, const TSample& v2) const;

	/// Returns the codewords of a list of samples.
	const vector<TCode> Encode(const vector<TSample>& vSamples) const;


	/// Train the quantizer using given set of samples.
	virtual void Train(const vector<TSample>& vSamples) = 0;

	/// Returns the codewords of a sample.
	virtual TCode Encode(const TSample& vSample) const = 0;

	/// Reconstructs a vector from a given code
	virtual TSample Reconstruct(const TCode& code1) const = 0;

	/// Returns the asymmetric distance between a preset query and a code.
	virtual double AsymmetricDistance(const TCode& code1) const = 0;

	/// Reads a quantizer from a binary file.
	virtual void Read(const string& file) = 0;

	/// Writes the quantizer to a binary file.
	virtual void Write(const string& file) const = 0;

	/// Returns a string representation for the name of the quantizer type.
	virtual string QuantizerTypeName() const = 0 ;

	/// Sets cached query
	virtual void SetQuery(const TSample& vSample){m_vQuery=vSample;}

protected:

	/// The query sample
	TSample m_vQuery;
} ;

