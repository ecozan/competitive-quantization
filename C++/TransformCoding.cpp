/*
 * TransformCoding.cpp
 *
 *  Created on: 24 May 2017
 *      Author: ozan
 */

#include "TransformCoding.h"
#include "MaxLloyd.h"

TransformCoding::TransformCoding(int j)
:Quantizer(),m_nJ(j)
{
}

TransformCoding::~TransformCoding()
{
}

void TransformCoding::Train(const vector<TSample>& vSamples)
{
	//Initialize
	m_PCA.Analyze(vSamples,m_nJ);
	CalculateCodeLenghts();
	CalculateCentersLloyd(vSamples);
}

Quantizer::TCode TransformCoding::Encode(const TSample& vSample) const
{
	Quantizer::TSample vTransform = m_PCA.Transform(vSample);

	return LloydQuantization(vTransform);;
}

Quantizer::TSample TransformCoding::Reconstruct(const TCode& code1) const
{
	TSample vReconstructed(code1.size());
	for(int i=0;i<code1.size();i++)
		vReconstructed[i] = m_vCenters[i][code1[i]];
	vReconstructed.resize(m_nJ,0);
	return m_PCA.Revert(vReconstructed);;
}

double TransformCoding::AsymmetricDistance(const TCode& code1) const
{
	if(m_vQuery.size()==0)
		throw Error("[SKPCAEmbedding] m_vQuery is not set!");

	double dist = 0;
	for(int i=0;i<code1.size();i++)
		dist+=pow((m_vQuery[i]-m_vCenters[i][code1[i]]),2);

	return sqrt(dist+m_dSubspaceDist*m_dSubspaceDist);
}

void TransformCoding::Read(const string& file)
{
	BinaryIn input(file);
	string strMark ;
	input.Read(strMark) ;
	if(strMark != QuantizerTypeName())
		throw Error(strMark+" "+ QuantizerTypeName() +">not found in " + file) ;

	input.Read(m_vCenters) ;
	input.Read(m_vCodeLengths) ;

	vector<vector<uint8_t> > vBytes ;
	input.Read(vBytes) ;
	string temp = BinaryOut::TemporaryFile() ;
	{
		BinaryOut output(temp) ;
		output.WriteBytes(vBytes[0]) ;
	}
	m_PCA.Read(temp) ;
	BinaryIn::Remove(temp) ;
//	m_nJ = m_PCA.GetOutputDimension();
}

void TransformCoding::Write(const string& file) const
{
	BinaryOut output(file);
	output.Write(QuantizerTypeName()) ;
	output.Write(m_vCenters);
	output.Write(m_vCodeLengths);

	vector<vector<uint8_t> > vBytes ;
	string temp = BinaryOut::TemporaryFile() ;
	m_PCA.Write(temp) ;
	{
		BinaryIn input(temp) ;
		vBytes.push_back(input.ReadBytes()) ;
	}
	BinaryIn::Remove(temp) ;
	output.Write(vBytes) ;
}

string TransformCoding::QuantizerTypeName() const
{
	return "TransformCoding";
}

void TransformCoding::SetQuery(const TSample& vSample)
{
	m_vQuery = m_PCA.Transform(vSample);
	m_dSubspaceDist = L2(vSample,m_PCA.Revert(m_PCA.Transform(vSample)));
}

void TransformCoding::CalculateCodeLenghts()
{
	m_vCodeLengths.clear();
	vector<double> vPrincipalWeights = m_PCA.GetPrincipalWeights();
	for(int i=0;i<vPrincipalWeights.size();i++)
		vPrincipalWeights[i] = sqrt(vPrincipalWeights[i]);

	Greedy(vPrincipalWeights);

//	m_PCA.SetOutputDimension(m_vCodeLengths.size());
	cout<<"Output dimension: "<<m_vCodeLengths.size()<<endl;
}

void TransformCoding::Greedy(const vector<double>& vPrincipalWeights)
{
	m_vCodeLengths.clear();
	vector<double> vTmpWeights = vPrincipalWeights;
	for(uint j=0;j<vTmpWeights.size();j++)
		vTmpWeights[j] = log((vPrincipalWeights[j]))/log(2);
	m_vCodeLengths.resize(m_nJ);
	for(int j=0;j<m_nJ;j++)
	{
		int maxAt = MaxAt(vTmpWeights);
		m_vCodeLengths[maxAt]++;
		vTmpWeights[maxAt] -= 1;
	}

	for(int j=0;j<m_nJ;j++)
		if(m_vCodeLengths[j]==0)
		{
			m_vCodeLengths.resize(j);
			break;
		}
}

void TransformCoding::CalculateCentersLloyd(const vector<TSample>& vSamples)
{
	m_vCenters.clear();
	m_vCenters.resize(m_vCodeLengths.size());

	vector<TSample> vTransFeatures(vSamples.size());
	for(int i=0;i<vSamples.size();i++)
		vTransFeatures[i] = m_PCA.Transform(vSamples[i]);

	for(int j=0;j<m_vCodeLengths.size();j++)
	{
		vector<double> vTransValues;
		vTransValues.reserve(vSamples.size());

		for(int i=0;i<vSamples.size();i++)
			vTransValues.push_back(vTransFeatures[i][j]);

		MaxLloyd mlQ(pow(2,m_vCodeLengths[j]));
		mlQ.Train(vTransValues);
		vector<double> vClusterCenters = mlQ.GetCenters();
		sort(vClusterCenters.begin(),vClusterCenters.end());
		m_vCenters[j] = vClusterCenters;
	}
}

Quantizer::TCode TransformCoding::LloydQuantization(const TSample& vSample) const
{
	Quantizer::TCode vCodes;
	vCodes.reserve(m_vCenters.size());

	for(int i=0;i<m_vCenters.size();i++)
	{
		double val = vSample[i];
		vector<double> vAbsVal(m_vCenters[i].size());
		for(int j=0;j<m_vCenters[i].size();j++)
			vAbsVal[j] = abs(m_vCenters[i][j]-val);

		double min = 10000000;
		int minAt = 0;
		for(int j=0;j<vAbsVal.size();j++)
			if(vAbsVal[j]<min)
			{
				min = vAbsVal[j];
				minAt = j;
			}
		vCodes.push_back(Quantizer::TCodeCore(minAt));
	}

	return vCodes;
}
