/*
 * KMeans.cpp
 *
 *  Created on: 19 May 2017
 *      Author: ozan
 */

#include "KMeans.h"
#include <thread>
#include <functional>
#include <iostream>

using namespace utilities;

KMeans::KMeans(int k, bool multiThread)
:m_nK(k),m_bMultiThread(multiThread)
{
	m_dSSD = 0;
}

KMeans::~KMeans()
{
}

void KMeans::Train(const vector<Quantizer::TSample>& vSamples,int nIterations)
{
	//Initialize
	InitializeCentroidsRandom(vSamples);

	for(int it=0;it<nIterations;it++)
	{
		UpdateClusterIndices(vSamples);
		UpdateClusterCentroids(vSamples);
	}
	UpdateClusterIndices(vSamples);
	for(int i=0;i<m_nK;i++)
		if(m_vCentroidPopulation[i]==0)
			std::cout<<"Centroid "<<i<<" is empty"<<std::endl;

}

void KMeans::InitializeCentroidsRandom(const vector<Quantizer::TSample>& vSamples)
{
	m_vCentroids.clear();
	m_vCentroids.reserve(m_nK);
	vector<Quantizer::TSample> vTmpSamples = vSamples;
	for(int i=0;i<m_nK;i++)
	{
		int randIndex = rand() % vTmpSamples.size() ;
		m_vCentroids.push_back(vTmpSamples[randIndex]);
		vTmpSamples.erase(vTmpSamples.begin()+randIndex);
	}
}

void KMeans::InitializeCentroids(const vector<Quantizer::TSample>& vSamples)
{
	vector<double> minDistance(vSamples.size(), 0) ;
	vector<double> minDistanceTemp(vSamples.size()) ;
	m_vCentroids.clear();
	m_vCentroids.reserve(m_nK);

	int nInitialCenterTrials = 8;
	// Choose a random vector for initial cluster.
	int randIndex = rand() % vSamples.size() ;
	m_vCentroids.push_back(vSamples[randIndex]);

	// Calculate initial distances and potential.
	double currentPotential = 0 ;
	for(int j = 0 ; j < vSamples.size() ; ++j)
	{
		double distanceSqr = pow(L2(vSamples[j],m_vCentroids[0]),2.0) ;
		minDistance[j] = distanceSqr;
		currentPotential += distanceSqr ;
	}

	// Choose remaining cluster means.
	for(int k = 1 ; k < m_nK ; ++k)
	{
		double bestPotential = -1 ;
		int bestIndex = -1 ;
		for(int i = 0 ; i < nInitialCenterTrials ; ++i)
		{
			// Choose center.
			double randPotential = double( (rand() | (rand() << 16 )) )/RAND_MAX * currentPotential ;
			int randIndex = 0 ;
			for( ; randIndex < vSamples.size() - 1 ; ++randIndex)
			{
				if(randPotential < minDistance[randIndex])
					break ;
				else
					randPotential -= minDistance[randIndex] ;
			}

			// Is a better potential produced than for previously chosen means?
			double thisPotential = 0 ;
			for(int j = 0 ; j < vSamples.size() ; ++j)
			{
				double distanceSqr = pow(L2(vSamples[j],vSamples[randIndex]),2);
				double currentDistance = min(distanceSqr, minDistance[j]) ;

				thisPotential += currentDistance ;
				minDistanceTemp[j] = currentDistance ;
			}
			if(bestPotential < 0 || thisPotential < bestPotential)
			{
				bestPotential = thisPotential ;
				bestIndex = randIndex ;
			}
		}

		for(int j = 0 ; j < vSamples.size() ; ++j)
		{
			minDistance[j] = minDistanceTemp[j] ;
		}
		m_vCentroids.push_back(vSamples[bestIndex]);
		currentPotential = bestPotential ;
	}
}
double KMeans::UpdateClusterIndices(const vector<Quantizer::TSample>& vSamples)
{
	int nThreads = 1;
	//TODO Set number of threads
	if(m_bMultiThread)
		nThreads = 8;

	vector<std::thread *> vThread(nThreads);
	vector<double> vSSDs(nThreads);
	vector<vector<int> > vClusterLabels(nThreads);
	int nSamplePerTask = vSamples.size()/nThreads;
	vector<distArgs> vDistArgs(nThreads);

	for(int i=0;i<nThreads;i++)
	{
		vDistArgs[i].nBegin = nSamplePerTask*i;
		vDistArgs[i].nEnd = nSamplePerTask*(i+1);

		if(abs(int(vSamples.size()-vDistArgs[i].nEnd))<nSamplePerTask)
			vDistArgs[i].nEnd = vSamples.size();

		vDistArgs[i].vCentroids = &m_vCentroids;
		vDistArgs[i].vSamples = &vSamples;
		vDistArgs[i].vClusterLabels.resize(vDistArgs[i].nEnd-vDistArgs[i].nBegin);
		vDistArgs[i].dSSD = 0;


		vThread[i] = new std::thread(&KMeans::DistanceToCentroid,this,std::ref(vDistArgs[i]));
	}

	for(int i=0;i<nThreads;i++)
		vThread[i]->join();

	m_vClusterLabels.clear();
	m_dSSD=0;
	for(int i=0;i<nThreads;i++)
	{
		m_dSSD+=vDistArgs[i].dSSD;
		m_vClusterLabels.insert(m_vClusterLabels.end(),(vDistArgs[i].vClusterLabels).begin(),(vDistArgs[i].vClusterLabels).end());
		delete vThread[i];
	}

	m_vCentroidPopulation.clear();
	m_vCentroidPopulation.resize(m_nK,0);
	for(int i=0;i<vSamples.size();i++)
		m_vCentroidPopulation[m_vClusterLabels[i]]++;

	m_dSSD /=vSamples.size();
//	cout<<"Total SSD "<<m_dSSD<<endl;

	return m_dSSD;
}

void KMeans::UpdateClusterCentroids(const vector<Quantizer::TSample>& vSamples)
{

	vector<Quantizer::TSample> vTmpCentroids(m_nK);
	for(int i=0;i<vTmpCentroids.size();i++)
		vTmpCentroids[i].resize(vSamples[0].size(),0);

	for(int i=0;i<vSamples.size();i++)
	{
		vTmpCentroids[m_vClusterLabels[i]]+=vSamples[i];
	}

	for(int i=0;i<m_vCentroids.size();i++)
		if(m_vCentroidPopulation[i]!=0)
			m_vCentroids[i] = vTmpCentroids[i]/double(m_vCentroidPopulation[i]);
//		else
//			cout<<"Centroid "<<i<<" is empty"<<endl;
}

void KMeans::DistanceToCentroid(distArgs& args) const
{
	for(int i=args.nBegin;i<args.nEnd;i++)
	{
		double dist = 100000000;
		int index = 0;
		for(int j=0;j<args.vCentroids->size();j++)
		{
			double tmpDist = L2((*args.vSamples)[i],m_vCentroids[j]);
			if(tmpDist<dist)
			{
				dist = tmpDist;
				index = j;
			}
		}

		args.vClusterLabels[i-args.nBegin]=index;
		args.dSSD += dist*dist;
	}
}
