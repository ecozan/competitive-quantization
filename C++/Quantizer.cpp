/*!
*************************************************************************************
 * \file Quantizer.cpp
 *
 * \brief
 *    Implementation of Quantizer base class.
 *

 *************************************************************************************
 */

#include "Quantizer.h"

Quantizer::Quantizer()
{
}

Quantizer::~Quantizer()
{}

const vector<Quantizer::TCode> Quantizer::Encode(const vector<TSample>& vSamples) const
{
	vector<TCode> vCodes(vSamples.size()) ;
	for(int i = 0 ; i < int(vSamples.size()) ; i += 1)
		vCodes[i] = Encode(vSamples[i]) ;
	return vCodes ;
}

const vector<double> Quantizer::AsymmetricDistance(const vector<TCode>& vCodes) const
{
	vector<double> vDistances(vCodes.size()) ;
	for(int i = 0 ; i < int(vCodes.size()) ; i += 1)
		vDistances[i] = AsymmetricDistance(vCodes[i]) ;
	return vDistances ;
}

double Quantizer::ReconstructedAsymmetricDistance(const TCode& code1, const TSample& v2) const
{
	TSample vReconstructed = Reconstruct(code1);
	return L2(v2,vReconstructed);
}
