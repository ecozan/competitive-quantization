This project implements the Competitive Quantization method as described in

"Ozan, E.C., Kiranyaz, S. and Gabbouj, M., 2016. Competitive Quantization for Approximate Nearest Neighbor Search. IEEE Transactions on Knowledge and Data Engineering, 28(11), pp.2884-2894."


How to use:

Use the function Train to train the quantizer on a dataset, Write/Save and Read/Load to save and load the trained quantizer.
Encode encodes a given sample to its corresponding binary code.
Asymmetric distance calculates the approximate distance of a code to a given query. 
The given query should be set using SetQuery function before Asymmetric Distance calculation.
Reconstruct function reconstructs a vector from a given code


For C++ version:

It requires Armadillo linear algebra library for eigenvalue decomposition which can be obtained from http://arma.sourceforge.net/
Required compile flags: -DARMA_DONT_USE_WRAPPER -std=c++11 -pthread


For python version:

It requires numpy, sklearn.cluster and sklearn.decomposition.


Send emails to ezgicanozan@gmail.com for any questions or bug reports.

All code in this repository is free to use for research purposes only.
